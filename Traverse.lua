
local Traverse = advancedMacros.inherit()

local _new = Traverse.new
function Traverse:new( )
  local obj = _new( self )
  return obj
end

function Traverse:WalkToXZ( Baritone, x, z )
  Baritone:XZ(x, z)
  return true;
end

function Traverse:Harvest( Baritone, x, z, nodeX, nodeY, nodeZ )
  log(Baritone)
  Baritone:XZ(x, z)
  sleep(500)
  lookAt(nodeX, nodeY, nodeZ, 450)
  sleep(500)
  lookAt(nodeX, nodeY, nodeZ, 100)
  attack()
  sleep(1000)
  lookAt(nodeX, nodeY, nodeZ, 100)
  attack()
  sleep(1000)
  lookAt(nodeX, nodeY, nodeZ, 100)
  attack()
  sleep(8000)
  return true
end

package.loaded.Traverse = Traverse
return Traverse