
local Fishing = advancedMacros.inherit()

local _new = Fishing.new
function Fishing:new( )
  local obj = _new( self )
  return obj
end

function Fishing:FishGudgeon(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse:WalkToXZ(Baritone, 0, 0)
  Traverse:Harvest(Baritone, 5, 5, 3.5, 6.5, 5.5)
  Traverse:Harvest(Baritone, 5, 8, 3.5, 6.5, 8.5)
  Traverse:Harvest(Baritone, 5, 10, 7.5, 6.5, 10.5)
end

function Fishing:FishTrout(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishSalmon(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishCarp(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishIceFish(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishPiranha(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishKoi(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishGylia(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishBass(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishMoltenEel(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishStarfish(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

function Fishing:FishDernicFish(RouteIdentifier, Baritone, Traverse)
  -- print("Preparing Collect Route: ", RouteIdentifier)
  Traverse.WalkToXZ(Baritone, 100, 200)
end

package.loaded.Fishing = Fishing
return Fishing