
local Baritone = advancedMacros.inherit()

local _new = Baritone.new
function Baritone:new( )
  local obj = _new( self )
  return obj
end

function GetPrimary ()
  local BaritoneAPI = luajava.bindClass( "baritone.api.BaritoneAPI" )
  local Prim = BaritoneAPI:getProvider():getPrimaryBaritone()
  return Prim
end

function Baritone:HasPath ()
  local BPath = GetPrimary():getPathingBehavior()
  return BPath:isPathing() or BPath:hasPath()
end

function Baritone:XZ ( x, z )
  local Prim = GetPrimary()
  local Goal = luajava.newInstance( "baritone.api.pathing.goals.GoalXZ", x, z )
  runOnMC(function() Prim:getCustomGoalProcess():setGoalAndPath(Goal) end)
  while (Baritone:HasPath()) do
    sleep(500)
  end
  return
end

package.loaded.Baritone = Baritone
return Baritone