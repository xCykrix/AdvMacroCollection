local IBaritone = require "Baritone"
local ITraverse = require "Traverse"
local Baritone = IBaritone:new()
local Traverse = ITraverse:new()

-- Initialize the classes for route running.
local IFishing = require "Fishing"
local Fishing = IFishing:new()

-- Prepare and initialize the route based on input.
local RouteIdentifier = "FishGudgeon" -- prompt("Please select the run path:")

-- https://forums.wynncraft.com/threads/list-of-places-to-gather-materials.239484/
-- Hand to the specified route to begin travel and processing.
if ( string.find(RouteIdentifier, 'Fish') ) then
  Fishing:FishGudgeon(RouteIdentifier, Baritone, Traverse)
end
